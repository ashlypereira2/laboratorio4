package com.example.laboratorio3parte1
import android.app.Application
import androidx.room.Room

class ComidaApplication: Application() {
    companion object{
        lateinit var db :AppDataBase
    }
    override fun onCreate() {
        super.onCreate()
        db =
            Room.databaseBuilder(applicationContext,AppDataBase::class.java,
                "comida.db"
            )
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
    }
}
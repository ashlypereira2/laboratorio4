package com.example.laboratorio3parte1

import android.content.Context
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import java.util.Date

class MainActivity : AppCompatActivity() {
    private var n:Int =0
    private var b:Boolean=false
    private lateinit var comidasAdapter: ComidaAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var botonAgregar: Button = findViewById<Button>(R.id.btnAdd)
        val r = findViewById<RecyclerView>(R.id.recycler1)


        val layout = LinearLayoutManager(applicationContext)
        layout.orientation = LinearLayoutManager.VERTICAL
        r.layoutManager = layout

        var comidaList : List<comida> =
            ComidaApplication.db.comidaDao().selectComidas()

        n=comidaList.size
        val comidasAdapter = ComidaAdapter(applicationContext,(comidaList as MutableList<comida>))
        r.adapter = comidasAdapter

        for (p in comidaList){
            println(p.nombre)
        }

        botonAgregar.setOnClickListener(){
            showInputDialog(this)
            println("cuadro de dialogo")
        }


    }

    fun showInputDialog(context: Context) {

        val editText = EditText(context)
        val editTextDesc = EditText(context)
        val editTextPrecio = EditText(context)


        val dialogBuilder = AlertDialog.Builder(context)
        dialogBuilder.setTitle("Ingrese datos de la comida")

        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.setPadding(30, 5, 30, 30)
        layout.addView(editText)
        layout.addView(editTextDesc)
        layout.addView(editTextPrecio)

        editText.hint= "nombre"
        editTextDesc.hint = "descripcion"
        editTextPrecio.hint = "precio"
        dialogBuilder.setView(layout)


        dialogBuilder.setPositiveButton("Aceptar") { dialog, _ ->
            val nombre = editText.text.toString() // Obtener el texto ingresado
            val descripcion = editTextDesc.text.toString()
            val precio = editTextPrecio.text.toString()

            val nuevaComida=comida(Date().time,nombre, descripcion, precio.toInt())



            ComidaApplication.db.comidaDao().insertComida(nuevaComida)
            dialog.dismiss()
        }


        dialogBuilder.setNegativeButton("Cancelar") { dialog, _ ->
            dialog.dismiss()
        }

        val dialog = dialogBuilder.create()
        dialog.show()
    }
    fun edit(id:Long){
        println("XD $id")
    }

}
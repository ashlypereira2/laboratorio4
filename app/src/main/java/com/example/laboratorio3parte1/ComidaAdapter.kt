package com.example.laboratorio3parte1

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView

import java.util.Date

class ComidaAdapter (ctx: Context, private val comidaModel: MutableList<comida> ): RecyclerView.Adapter<ComidaAdapter.comidasViewHolder>(){
    inner class comidasViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        var imageUrl = itemView.findViewById<ImageView>(R.id.imageLogo)
        var titulo = itemView.findViewById<TextView>(R.id.txtTitulo)
        var descripcion = itemView.findViewById<TextView>(R.id.txtDescripcion)
        var btnEliminar= itemView.findViewById<Button>(R.id.btnEliminar)
        var btnModificar= itemView.findViewById<Button>(R.id.btnModificar)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): comidasViewHolder {
        val v = LayoutInflater.from (parent.context).inflate(R.layout.item_view,parent,false)
        return comidasViewHolder(v)
    }
    override fun getItemCount(): Int {
        return comidaModel.size
    }
    override fun onBindViewHolder(holder: comidasViewHolder, position: Int) {
        val i = comidaModel[position]
        holder.titulo.text = i.nombre
        holder.descripcion.text = i.descripcion

        val imageUrl = i.imagen

        holder.btnEliminar.setOnClickListener {
            // Captura los datos de la comida correspondiente y muestra un log
            Log.i("eliminar", "Comida seleccionada:id:${i.id} con posicion $position")
            var idDelete=i.id

            //(holder.itemView.context as MainActivity).removeComida(position)
            val comidaDao = ComidaApplication.db.comidaDao()

            notifyItemRemoved(idDelete.toInt())
            notifyItemRangeChanged(position, comidaModel.size) // Ajustar las posiciones
            comidaDao.deleteComidaById(idDelete)
            println(comidaModel.toString())
        }
        holder.btnModificar.setOnClickListener(){
            val adapterPosition = holder.adapterPosition
            if (adapterPosition != RecyclerView.NO_POSITION) {
                val comida = comidaModel[adapterPosition]

                val dialog = AlertDialog.Builder(holder.itemView.context)
                val inflater = LayoutInflater.from(holder.itemView.context)
                val dialogView = inflater.inflate(R.layout.item_modificar, null)

                val editTextNuevoNombre = dialogView.findViewById<EditText>(R.id.editTextNuevoNombre)
                val editTextNuevaDescripcion = dialogView.findViewById<EditText>(R.id.editTextNuevaDescripcion)
                val editTextNuevoPrecio = dialogView.findViewById<EditText>(R.id.editTextNuevoPrecio)

                editTextNuevoNombre.setText(comida.nombre)
                editTextNuevaDescripcion.setText(comida.descripcion)
                editTextNuevoPrecio.setText(comida.precio.toString())

                dialog.setView(dialogView)

                dialog.setPositiveButton("Guardar") { _, _ ->

                    val nuevoNombre = editTextNuevoNombre.text.toString()
                    val nuevaDescripcion = editTextNuevaDescripcion.text.toString()
                    val nuevoPrecio = editTextNuevoPrecio.text.toString()

                    val nuevaComida = comida(comida.id, nuevoNombre, nuevaDescripcion, nuevoPrecio.toInt(), "")
                    comidaModel[adapterPosition] = nuevaComida
                    notifyItemChanged(adapterPosition)

                    val personasDao = ComidaApplication.db.comidaDao()
                    personasDao.updateComida(nuevaComida)
                }

                dialog.setNegativeButton("Cancelar") { dialog, _ ->
                    dialog.dismiss()
                }

                dialog.show()
            }

        }
    }
    fun addComida(newItem: comida) {
        this.comidaModel.add(newItem) // Agrega a la lista del adaptador
        notifyItemInserted(itemCount - 1) // Notifica la inserción
    }



}
